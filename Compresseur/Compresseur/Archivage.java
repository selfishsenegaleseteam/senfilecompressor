/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Compresseur;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author PamodZou Kana
 */
public class Archivage {
        public int nombreFichiers;
        public File[] mesFichiers;
        
        public int getNF(){
            return this.nombreFichiers;
        }
        //Constructeur
        Archivage(File[] mesFiles, int nbFiles){
            this.nombreFichiers=nbFiles;
            this.mesFichiers=new File[nombreFichiers];
            this.mesFichiers=mesFiles;
        }
        
        /*On pourrait aussi faire ça!. C'est même plus simple
        Archivage(File[] mesFiles){
            this.nombreFichiers=mesFiles.length;
            this.mesFichiers=new File[nombreFichiers];
        }*/
    
        public String getExtension(File file){
            String nomFichier=file.getName();
            int lastPos=nomFichier.lastIndexOf('.');
            String ext=nomFichier.substring(lastPos+1);
            return ext;
        }
        public String getName(File file){
            return file.getName();
        }
        
        public long getTaille(File file){
            return file.length();
        }
        
        public byte[] lireFichier(File f) throws FileNotFoundException, IOException{
            byte[] b=new byte[(int)this.getTaille(f)];
            FileInputStream fis=new FileInputStream(f);
            fis.read(b);
            fis.close();
            return b;
            
        }
        
        public String[] getExtensions(){
            String[] exts=new String[this.nombreFichiers];
            for(int j=0;j<this.nombreFichiers;j++){
                exts[j]=this.getExtension(this.mesFichiers[j]);
            }
            return exts;
        }
        public String[] getNames(){
            String[] names=new String[this.nombreFichiers];
            for(int j=0;j<this.nombreFichiers;j++){
                names[j]=this.getName(this.mesFichiers[j]);
            }
            return names;
        }
        
        
        public long[] getTailles(){
            long tailles[]=new long[this.nombreFichiers];
            for(int k=0;k<this.nombreFichiers;k++){
                tailles[k]=this.getTaille(this.mesFichiers[k]);
            }
            return tailles;
        }
        
        public int tailleExtensions(String[] extensions){
            int taille=0;
            for(int l=0;l<this.getNF();l++){
                taille+=extensions[l].length();
            }
            return taille;
        }
        
        public int tailleNames(String[] names){
            int taille=0;
            for(int l=0;l<this.getNF();l++){
                taille+=names[l].length();
            }
            return taille;
        }
        
        
        public long tailleTotale(){
            long totaly=0;
            for(int i=0;i<this.nombreFichiers;i++){
                totaly+=this.getTaille(mesFichiers[i]);
            }
            //on ajoute la taille du tableau de String
            //C'est pour la bleta d'extensions
            totaly+=this.tailleExtensions(this.getExtensions());
            //on ajoute la taille du tableau de String
            //C'est pour la bleta de noms
            totaly+=this.tailleNames(this.getNames());
            //La taille d'un double étant de 8 octets on ajoute 8*n
            //C'est pour la bleta de tailles
            totaly+=(8*this.nombreFichiers);
            //On y ajoute la taille de l'entier contenant this.nombreFichiers
            //Plus la taille de l'entier contenant la taille de extension
            return totaly+4+this.tailleExtensions(this.getExtensions());
        }
        
        public void ecrireExtensions(DataOutputStream dos) throws IOException{
            for(int i=0;i<this.nombreFichiers;i++){
                dos.writeUTF(this.getExtensions()[i]);
            }
        }
        
        public void ecrireNoms(DataOutputStream dos) throws IOException{
            for(int i=0;i<this.nombreFichiers;i++){
                dos.writeUTF(this.getNames()[i]);
            }
        }
        
        public void ecrireTailles(DataOutputStream dos) throws IOException{
            for(int i=0;i<this.nombreFichiers;i++){
                dos.writeLong(this.getTailles()[i]);
            }
        }
        public void writeAll(DataOutputStream dos) throws IOException{
            for(int i=0;i<this.nombreFichiers;i++){
                dos.write(this.lireFichier(this.mesFichiers[i]));
            }
        }
        
        public void mixItAll(String path) throws FileNotFoundException, IOException{
            //byte[] sortie=new byte[(int)this.tailleTotale()];
            File f=new File(path);
            FileOutputStream fis=new FileOutputStream(f);
            DataOutputStream dos = new DataOutputStream(fis);
            //On met le nombre de fichiers
            dos.write(this.nombreFichiers);
            //on met la taille des extensions
            dos.write(this.tailleExtensions(this.getExtensions()));
            //on met le tableau d'extensions
            this.ecrireExtensions(dos);
            //on met le tableau de tailles
            this.ecrireTailles(dos);
            //on met le tableau de noms
            this.ecrireNoms(dos);
            //on met les données
            this.writeAll(dos);
            dos.close();
            
        }
        
        
    
}
