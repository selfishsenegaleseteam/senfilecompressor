/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Compresseur;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 *
 * @author PamodZou Kana
 */
public class Desarchivage {
    
    public int nombreDeFichiers;
    public String[] extensions;
    public double[] tailles;
    public String[] noms;
    public int tailleExtensions;
    //Constructeur
    public Desarchivage(){
        
    }
    
    //Une methode qui permet d'initialiser les attributs
    public void init(File f) throws FileNotFoundException, IOException{
        FileInputStream fis=new FileInputStream(f);
        DataInputStream dis=new DataInputStream(fis);
        this.nombreDeFichiers=dis.read();
        this.tailleExtensions=dis.read();
        this.extensions=this.buildExtensions(dis);
        this.tailles=this.buildTailles(dis);
        this.noms=this.buildNames(dis);
        this.buildFiles(dis);
    }
    
    public String[] buildExtensions(DataInputStream dis) throws IOException{
        String[] extensions=new String[this.nombreDeFichiers];
        //On recupere les extensions
        for(int i=0;i<this.nombreDeFichiers;i++){
            extensions[i]=dis.readUTF();
        }
        return extensions;
    }
    public String[] buildNames(DataInputStream dis) throws IOException{
        String[] noms=new String[this.nombreDeFichiers];
        //On recupere les extensions
        for(int i=0;i<this.nombreDeFichiers;i++){
            noms[i]=dis.readUTF();
        }
        return noms;
    }
    public double[] buildTailles(DataInputStream dis) throws IOException{
        double[] tailles=new double[this.nombreDeFichiers];
        //On recupere les extensions
        for(int i=0;i<this.nombreDeFichiers;i++){
            tailles[i]=dis.readLong();
        }
        return tailles;
    }
    public void buildFiles(DataInputStream dis) throws IOException{
        int beg=0;
        byte[] contenu;
        File fichier;
        FileOutputStream fos;
        Compressor c=new Compressor();
        
        for(int i=0;i<this.nombreDeFichiers;i++){
            contenu=new byte[(int)this.tailles[i]];
            dis.read(contenu,beg,(int)this.tailles[i]);
            fichier=new File(this.noms[i]+"."+this.extensions[i]);
            fos=new FileOutputStream(fichier);
            fos.write(contenu);
            System.out.println("Decompression du fichier "+this.noms[i]+" en cours...");
            c.ini();
            c.decompress(this.noms[i]+"."+this.extensions[i], this.noms[i].replace(".part", ""));
            System.out.println("Fichier "+this.noms[i]+" decompresse");
            fos.close();
        }
    }
}
