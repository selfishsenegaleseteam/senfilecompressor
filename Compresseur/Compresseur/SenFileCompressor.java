/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Compresseur;

import java.io.File;
import java.io.IOException;

/**
 *
 * @author PamodZou Kana
 */
public class SenFileCompressor {
    
    
    public static String stringIze(String[] array){
        String output="";
        for(int i=0;i<array.length;i++){
            output+=array[i]+"!";
        }
        return output;
    }
    public static String getPath(String chaine, int index){
        String sortie="";
        int i=index+2;
        while((i<chaine.length())&&(!chaine.substring(i, i+1).equals("!"))){
            sortie+=chaine.substring(i, i+1);
            i++;
        }
        return sortie;
    }
    public static void init(){
        System.out.println(" ___  ____  _  _    ____  ____  __    ____     ___  _____  __  __  ____  ____  ____  ___  ___  _____  ____ ");
        System.out.println("/ __)( ___)( \\( )  ( ___)(_  _)(  )  ( ___)   / __)(  _  )(  \\/  )(  _ \\(  _ \\( ___)/ __)/ __)(  _  )(  _ \\");
        System.out.println("\\__ \\ )__)  )  (    )__)  _)(_  )(__  )__)   ( (__  )(_)(  )    (  )___/ )   / )__) \\__ \\__ \\ )(_)(  )   /");
        System.out.println("(___/(____)(_)\\_)  (__)  (____)(____)(____)   \\___)(_____)(_/\\/\\_)(__)  (_)\\_)(____)(___/(___/(_____)(_)\\_)");

    }
    public static void showHelp(){
     System.out.println("Java SenFileCompressor -c fic1 fic2 fic3 ... [ -r cheminVersRepertoire [-f] ] [-v] | -d fichier.sfc [-r cheminVersRepertoire [-f] ] [-v]| ");
     System.out.println("---------------------------------------------");
     System.out.println("Pour compresser une liste de fichiers: java SenFileCompressor -c <liste fichier a compresser> [-r le chemuin relatif utilise] [-v].\n Cette commande renvoie un fichier compresse d'extension .sfc");
     System.out.println("---------------------------------------------");
     System.out.println("Pour decompresser un fichier .sfc: java SenFileCompressor -d fichierADecompresser.sfc [-r le chemuin relatif utilise] [-v]");
     System.out.println("---------------------------------------------");
    }
    public static void main(String[] args) throws IOException{
        try{
        init();
        String arguments=stringIze(args);
        if((args.length==1)&&(args[0].equals("-h"))){
            showHelp();
        }
        if(args.length<=1){
            System.out.println("Pour afficher l'aide: java SenFileCompressor -h");
        }
        if(args.length>=2){
            
            if(args[0].equals("-c")){   
                Compressor c=new Compressor();
                c.ini();
               
                int r=arguments.indexOf("-r");
                int v=arguments.indexOf("-v");
                int f=arguments.indexOf("-f");
                String givenName="";
                //Si ni -r ni -v ne sont fournis comme arguments,
                if((r==-1)&&(v==-1)&&(f==-1)){
                    File[] mesFichiers=new File[args.length-1];
                    for(int i=1;i<args.length;i++){
                        c.compress(args[i],args[i]+".part");
                        mesFichiers[i-1]=new File(args[i]+".part");
                        System.out.println(args[i]);
                        givenName+="."+args[i];
                    }
                    Archivage a=new Archivage(mesFichiers,args.length-1);
                    a.mixItAll(givenName+".sfc");
                }
                if((r==-1)&&(v!=-1)&&(f==-1)){
                    File[] mesFichiers=new File[args.length-1];
                    for(int i=1;i<args.length;i++){
                        System.out.println("Le fichier "+args[i]+" est en cours de compression");
                        c.compress(args[i],args[i]+".part");
                        System.out.println("Compression achevee. Fichier disponible a: "+args[i]+".part");
                        mesFichiers[i-1]=new File(args[i]+".part");
                        givenName+="."+args[i];
                    }
                    Archivage a=new Archivage(mesFichiers,args.length-1);
                    System.out.println("Archivage en cours...");
                    a.mixItAll(givenName+".sfc");
                    System.out.println("Archivage acheve. Fichier disponible a: "+givenName+".sfc");
                }
                if((r!=-1)&&(v!=-1)&&(f==-1)){
                   
                    String path=getPath(arguments,r+1);
                    if(new File(path).isDirectory()){
                        File[] mesFichiers=new File[args.length-3];
                        givenName=path+"/";
                        for(int i=1;i<args.length-3;i++){
                            givenName+="."+args[i];
                            args[i]=path+"/"+args[i];
                            System.out.println(args[i]);
                            System.out.println("Le fichier "+args[i]+" est en cours de compression");
                            c.ini();
                            c.compress(args[i],args[i]+".part");
                            System.out.println("Compression achevee. Fichier disponible a: "+args[i]+".part");
                            mesFichiers[i-1]=new File(args[i]+".part");
                        }
                        Archivage a=new Archivage(mesFichiers,args.length-4);
                        System.out.println("Archivage en cours...");
                        a.mixItAll(givenName+".sfc");
                        System.out.println("Archivage acheve. Fichier disponible a: "+givenName+".sfc");
                    }
                    else
                    {
                        System.out.println(path+" n'est pas un chemin convenable");
                    }
                }
                if((r!=-1)&&(v==-1)&&(f==-1)){
                    String path=getPath(arguments,r+1);
                    if(new File(path).isDirectory()){
                        File[] mesFichiers=new File[args.length-2];
                        givenName=path+"/";
                        for(int i=1;i<args.length-2;i++){
                            givenName+="."+args[i];
                            args[i]=path+"/"+args[i];
                            System.out.println(args[i]);
                            c.compress(args[i],args[i]+".part");
                            mesFichiers[i-1]=new File(args[i]+".part");
                        }
                        Archivage a=new Archivage(mesFichiers,args.length-3);
                        a.mixItAll(givenName+".sfc");
                    }
                    else
                    {
                        System.out.println(path+" n'est pas un chemin convenable");
                    }
                }
                
            }
            
            else if(args[0].equals("-d")){
                Compressor c=new Compressor();
                c.ini();
                int r=arguments.indexOf("-r");
                int v=arguments.indexOf("-v");
                String givenName="";
                //Si ni -r ni -v ne sont fournis comme arguments,
                if((r==-1)&&(v==-1)){
                    Desarchivage d=new Desarchivage();
                    File f=new File(args[1]);
                    d.init(f);
                }
                if((r==-1)&&(v!=-1)){
                    Desarchivage d=new Desarchivage();
                    File f=new File(args[1]);
                    System.out.println("L'archivage a debute");
                    d.init(f);
                    System.out.println("Fin de l'archivage");
                    System.out.println("Les fichiers decompresses sont disponibles sur dossier de l'application");               
                }
                if((r!=-1)&&(v!=-1)){
                    Desarchivage d=new Desarchivage();
                    String path=getPath(arguments, r+1);
                    File f=new File(path+"/"+args[1]);
                    System.out.println("L'archivage a debute");
                    d.init(f);
                    System.out.println("Fin de l'archivage");
                    System.out.println("Les fichiers decompresses sont disponibles sur dossier de l'application");
                }
                if((r!=-1)&&(v==-1)){
                    Desarchivage d=new Desarchivage();
                    String path=getPath(arguments, r+1);
                    File f=new File(path+"/"+args[1]);
                    d.init(f);
                }
            }
            else{
                SenFileCompressor.showHelp();
            }
        }
        }catch(Exception e) 
        {
            String ScenarioDErreur=":( Une erreur independante de notre volonte s'est produite. Pour y remedier, voici quelques conseils: \nVerifie que -r est suivi d'un chemin correct ne se terminant par par /.\n Verifiez que l'argument fourni apres -r correspond a un repertoire auquel vous avez acces.\n Sinon, reexecutez cette console en mode administrateur.\n Assurez vous que les fichiers que vous voulez compresser existent bel et bien.\n  ";
            System.out.println(ScenarioDErreur);
            showHelp();
            /*Vous remarquere que ces algorithmes ne sont pas efficients pour le traitement d'images,de videos";
            et de sons...Ils sont optimisés pour le traitements de fichiers textes*/
            
        }
    }
}
